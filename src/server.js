/**
 * Node's http is a very low-level tool and requires a complex set of instructions.
 * For this reason developers normally use a framework that makes it easier, such as Express
 * https://blog.risingstack.com/your-first-node-js-http-server/
 * @author Marc Ferguson <marc@fergytech.com>
 * @file Creates a small web server to test the HTML files in this project.
 * @todo Build a tiny server without the use of a framework.
 */

 const http = require("http");
 const port = 8080;

 const requestHandler = (request, response) => {
     console.log(`Request made for ${request.url}`);
     response.end();
 }

 const server = http.createServer(requestHandler);
 
 server.listen(port, (err) => {
     if (err) {
        return console.error("Something isn't right.", err);
     }
     console.log(`Server is up and running on port ${port}.`);
 });