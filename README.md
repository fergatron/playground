# playground

This project is intended to keep me up-to-date with all the changes happening in front-end web development. The core languages to experiment with are: HTML, CSS, and JavaScript. The idea is to not use frameworks or libraries.